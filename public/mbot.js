(function (){
  const VERSION = 'v0.0.1';

  const DEFAULT_COMMANDS = [
    '// Coller votre programme ici'
  ];

  const NSIX_LOGIN_URL = 'http://app.nsix.fr/connexion';
  const SERVER_URL = 'https://webamc.nsix.fr';


  const _editor = document.getElementById('editor');

  // display current version
  document.getElementById('version').textContent = VERSION;

  // display error msg
  function error(msg) {
    let elt = document.getElementById('error');
    elt.innerText = msg;
    elt.style.display = 'inline-block';
  }

  async function getHexHash(algorithm) {
    if(algorithm === 'SHA-1') {
      return window.sha1(_checkImage.data);
    } else if(algorithm === 'SHA-256') {
      return window.sha256(_checkImage.data);
    }
    console.error('Unavailable algorithm', algorithm);
    return '';
    // Crypto isn't available when not in secure context (eg. iframe).
    // const hash = await crypto.subtle.digest(algorithm, _checkImage.data);
    // const hashArray = Array.from(new Uint8Array(hash));
    // return hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
  }

  function showSend() {
    let elt = document.getElementById('send');
    elt.style.display = 'none';
    elt = document.getElementById('sending');
    elt.style.display = 'inline-block';
  }
  function showStart() {
    const codeDisplay = document.getElementById('highlighting');
    let elt = document.getElementById('running');
    elt.style.display = 'none';
    elt = document.getElementById('send');
    elt.style.display = 'inline-block';
    elt = document.getElementById('error');
    elt.style.display = 'none';
    codeDisplay.style.display = 'none';
  }

  // starts engraving using editor content as commands
  function sendPrgm() {
    showSend();

    const token = getAuthToken();
    if(token) {
      const body = {
        'prgm': _editor.value
      };
      const req = new Request(SERVER_URL + '/mbot/send',  {
        'method': 'POST',
        'headers': {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        'body': JSON.stringify(body)
      });
      fetch(req).then(res => { return res.json(); })
      .then(data => {
        console.info(JSON.stringify(data));
        _user.results.push(data);
        updateAchievements();
      });
    }
  }

  function login() {
    const current = location.href;
    location.href = `${NSIX_LOGIN_URL}?dest=${current}`;
  }

  function getAuthToken(){
    let token = null;
    if(document.cookie) {
      const name = 'ember_simple_auth-session='
      let cookies = decodeURIComponent(document.cookie).split(';');
      for (let c of cookies) {
        let idx = c.indexOf(name);
        if(idx > -1) {
          let value = c.substring(name.length + idx);
          let json = JSON.parse(value);
          token = json.authenticated.access_token;
        }
      }
    }
    return token;
  }

  function loadUser(cb) {
    let token = getAuthToken();
    if(token) {
      const meUrl = SERVER_URL + '/students/profile';
      const req = new Request(meUrl);
      fetch(req, {
        'headers': {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      }).then(res => {
        let json = null;
        if(res.status === 200) {
          json = res.json();
        }
        return json;
      }).then(data => {
        console.info(JSON.stringify(data, '', ' '));
        console.info(data.student);
        cb(data.student);
      }).catch(err => {
        console.warn('Unable to fetch user', err);
        cb(null);
      });
    } else {
      cb(null);
    }
  }

  function logout() {
    const cookies = ['ember_simple_auth-session', 'ember_simple_auth-session-expiration_time'];
    for (let cookie of cookies) {
      document.cookie=`${cookie}=; domain=${COOKIE_DOMAIN}; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
    }
    location.reload();
  }

  function showLoading() {
    document.getElementById('loading').classList.remove('hidden');
  }

  function hideLoading() {
    document.getElementById('loading').classList.add('hidden');
  }

  // Display login required popup
  function loginRequired() {
    let lr = document.getElementById('login-required');
    lr.style.width = '100%';
    lr.onclick = hideLoginPopup;
    document.getElementById('login-popup').style.transform = 'translate(0,0)';
  }

  function hideLoginPopup() {
    document.getElementById('login-popup').style.transform = 'translate(0,-80vh)';
    document.getElementById('login-required').style.width = '0%';
  }

  let _user = null;

  function init(){
    if ( window.location !== window.parent.location ) {
      let elt = document.getElementById('title-header');
      elt.style.display = 'none';
    }

    document.getElementById('login').addEventListener('click', login);

    document.getElementById('username').innerHTML = "Captain"

    showLoading();
    loadUser((user) => {
      // TODO session cache
      if(user) {
        _user = user;
        document.getElementById('username').innerHTML = user.firstName || 'Moi';
      } else {
        loginRequired();
        _user = null;
      }

      hideLoading();
    });
  }

  init();

  window.sendPrgm = sendPrgm;
})();